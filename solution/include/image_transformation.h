#ifndef GITHUB_IMAGE_TRANSFORMATION_H
#define GITHUB_IMAGE_TRANSFORMATION_H

#include "abstract_image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image source);

#endif //GITHUB_IMAGE_TRANSFORMATION_H
